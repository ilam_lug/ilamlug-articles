#### Useful Commands

------

<!-- In Colab -->
<!-- ! python3 -m pip install --force-reinstall https://github.com/yt-dlp/yt-dlp/archive/master.tar.gz -->

<!-- !yt-dlp --add-metadata --embed-subs --output '%(title)s.%(ext)s' -f 'bestvideo+bestaudio' --merge-output-format mkv  https://www.youtube.com/watch?v=3VULmrB7qTU -->

-   [yt-dlp](https://github.com/yt-dlp/yt-dlp/wiki/Installation)
```bash
yt-dlp --force-ipv4 --sub-langs en --write-subs --write-auto-sub --add-metadata --embed-subs --output '%(title)s.%(ext)s' -f 'bestvideo+bestaudio' --merge-output-format mkv "yt_playlist_video_url"
```

-   --download-archive archive.txt
-   This will make a txt file that keeps an archive of what you downloaded. Then whenever you download again it references that same file and checks to make sure you haven’t yet.

**Downlaoding from Youtube and more- `youtube-dl`:** 

-   using command-line `until` keyword
-   starting from 1st in the playlist
-   if any errors occur wait for 10 seconds
-   creates directory name and each video with it's playlist index and file exention, nothing more.

```bash
    until torify youtube-dl --playlist-start 1  -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' "https://www.youtube.com/playlist?list=PLAt10Vana3Ydk4KFbhBuZ0jFm74Tg0RAG" ; do sleep 10 ; done ;
```

- Socks5 proxy
- continue warnings and errors
- best format available
- download subtitle `en`

```bash
    youtube-dl --proxy socks5://127.0.0.1:8080 --continue --ignore-errors --format best  --write-sub --sub-lang en  -o './%(playlist_index)s - %(title)s.%(ext)s' https://www.youtube.com/playlist?list=PLAt10Vana3Ydk4KFbhBuZ0jFm74Tg0RAG
```

- gather best `audio` and `video` and merge them via `FFmpeg` 

```bash
    youtube-dl --format bestvideo+bestaudio https://www.youtube.com/watch?v=fuHM-PPUDU0
```

- `mp4` format

```bash
    youtube-dl -i -f mp4 --yes-playlist 'https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv'
```

- list `subtitles` for a `youtube link`

```bash
    youtube-dl --list-subs https://www.youtube.com/watch?v=Ye8mB6VsUHw
```

- only `subtitles` in `english`

```bash
    youtube-dl --write-sub --sub-lang en --skip-download 'https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv'
```

- Best `Audio` with `m4a` exention
- Best `Video` with `mp4` exention
- Merge with `FFmpeg` as `MP4`
- Burn `Subtitle` into Output 

```bash
    youtube-dl --sub-lang en --write-sub -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --embed-subs --write-sub  --merge-output-format mp4  "https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv"
```

- extract only Audio with `best quality (0)`

```bash
    youtube-dl --extract-audio --audio-format mp3  --audio-quality  0 https://www.youtube.com/watch?v=dczAw7aUjDE
```

- Another variation

```bash
    youtube-dl --continue --ignore-errors --output '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --merge-output-format mp4 'https://www.youtube.com/playlist?list=PLB601A45937E23146' -vvvv
```

-   Owner prefered one.
```bash
youtube-dl --force-ipv4 --add-metadata --write-srt --sub-lang en --output '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' -f 'bestvideo+bestaudio' -merge-output-format mp4 "[URL HERE TO CHANNELS PLAYLISTS]" -vvv
```


-   **verfication needed**

-   Ultimate Script

```bash
youtube-dl --download-archive "archive.log" -i --add-metadata --all-subs --embed-subs --embed-thumbnail --match-filter "playlist_title != 'Liked videos' & playlist_title != 'Favorites'" -f "(bestvideo[vcodec^=av01][height>=1080][fps>30]/bestvideo[vcodec=vp9.2][height>=1080][fps>30]/bestvideo[vcodec=vp9][height>=1080][fps>30]/bestvideo[vcodec^=av01][height>=1080]/bestvideo[vcodec=vp9.2][height>=1080]/bestvideo[vcodec=vp9][height>=1080]/bestvideo[height>=1080]/bestvideo[vcodec^=av01][height>=720][fps>30]/bestvideo[vcodec=vp9.2][height>=720][fps>30]/bestvideo[vcodec=vp9][height>=720][fps>30]/bestvideo[vcodec^=av01][height>=720]/bestvideo[vcodec=vp9.2][height>=720]/bestvideo[vcodec=vp9][height>=720]/bestvideo[height>=720]/bestvideo)+(bestaudio[acodec=opus]/bestaudio)/best" --merge-output-format mkv -o "%cd%/%%(playlist_uploader)s/%%(playlist)s/%%(playlist_index)s - %%(title)s - %%(id)s.%%(ext)s" "[URL HERE TO CHANNELS PLAYLISTS]" 
```

-   `Mute` with `FFmpeg`
```bash
    ffmpeg -i source.mkv -codec copy -an muted_source.mkv
```

-   `Combine` audio and video into one file using `FFMpeg`
```bash
    ffmpeg -i muted_source.mkv -i optimized_audio.mp3 -codec copy output_video.mp4
```

-   capture screen via `ffmpeg`
```bash
ffmpeg -y  -f x11grab -framerate 60  -s $(xdpyinfo | grep dimensions | awk '{print $2;}') -i $DISPLAY -f alsa -i default -r 30  -c:v h264 -crf 0 -preset ultrafast -c:a aac "output.mp4"
```

-   move srt to MP4 
```bash
ffmpeg -i input.mp4 -f srt -i input.srt \
-map 0:0 -map 0:1 -map 1:0 -c:v copy -c:a copy \
-c:s mov_text output.mp4
```

-   move srt to MKV
```bash
ffmpeg -i input.mp4 -f srt -i input.srt \
-map 0:0 -map 0:1 -map 1:0 -c:v copy -c:a copy \
-c:s srt  output.mkv
```

-   extract audio (mp3) from video container.
```bash
ffmpeg -i "${FILE}" -vn -ab 128k -ar 44100 -y "${FILE%.webm}.mp3
```

-   Using ffmpeg, extract audio streams from video file and output to single stereo wav file
-   -i option -> [The 0:1:0] seems to represent input1, stream1 (the track), channel (left/right if it were stereo).
```bash
ffmpeg -i test2.mxf -filter_complex "[0:1:0][0:2:0]amerge=inputs=2[aout]" -map "[aout]" output.wav
```

-   Extract the audio stream without re-encoding as aac
-   vn -> no video
-   `Copy is fast because to copies entire groups of frames. A group of frames (GOP) can not be split up. If you want to copy frames from a GOP, but not the entire GOP, you must reencode, Which is slower.`
-   `if you ever work with videos - you would have to use -c:a copy to only copy the audio codec, like -acodec`
-   `-c` or `-codec` is a generic stream selector, so you can use it to set the codec for any of the streams be they audio or video.
-   `-acodec` is a subset of that functionality that automatically scopes to Audio streams.
-   `-acodec:1` is the same as `-codec:a:1` and indicates you are setting the codec for the second audio stream (the first audio stream is 0).
-   Also consider `ffprobe`
```bash
ffmpeg -i input-video.avi -vn -acodec copy output-audio.aac
```


-   remove pdf permissions via ghost script
```bash
gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile="input_unencrypted.pdf" -f "input_encrypted.pdf"
```

---

**Cure with Curl**

-   
-   
```bash
curl  -v --tlsv1.3 'URI'

curl --proto '=https' --tlsv1.2 'URI'

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs
```


**Download from GDrive**

-   `curl` cookie
-   `curl` file

```bash
    fileId=1bNwTqq7bbVTfwdX0LNcl8ZomwqG2xfDc
    fileName=VSCode_Power_User.zip
    curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null
    code="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"
    curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${code}&id=${fileId}" -o ${fileName}
```

**Download Specefic Files**

-   a complex and slow `wget` command to index them. 
-   and redirect it to a file like this
```bash
    wget --spider -r --accept mkv,mp4,avi,srt --no-parent -l 200 -e robots=off 'http://86.146.64.205:8800/Films/' 2>&1 | egrep -o -e'(^.*http:.*[^/]$)' | awk -F ' ' '{print $2}'
```

-   Then you remove specific files you want to ignore with your favorite editor.
-   `wget` with this input file.
```bash
    wget -x -i urls.txt
```

**Powerful Download manager**

-   Download links from file with `xargs`
```bash
    cat file.urllist.txt | xargs axel -n 15 -q
```

```bash
cat links | xargs -d '\n' -l1 axel -akn 16
```


-   Download through `loop`
```bash
    while read url; do axel $url; done < mylinks.txt
```

**Bootstraping `SSH` key for `GIT`**

-   Create `ed25519` key
-   `Edwards-curve Digital Signature Algorithm` is a `digital signature` scheme using a variant of Schnorr signature based on twisted Edwards curves.
-   `RFC 8032`
```bash
    ssh-keygen -t ed25519 -C "email_registered@gitlab.com"
```

-   Add your SSH private key to the ssh-agent
```bash
    ssh-add address_of_your_private_key
```

-   Check via Psudo Shell that you can connect
```bash
    ssh -T git@gitlab.com 
```

-   now you can clone, push and pull
```bash
    git clone git@gitlab.com:myrepo/myrepo.git 
```

**Pretty Goog Privacy with `gnugp` package or `gpg` command**

-   Setup GPG Keys
```bash
    gpg --full-generate-key
```

-   Export Your Key as `your.name.gpg.asc`
-   Send key to KeyServer
-   keyserver:  e.g. https://keys.openpgp.org
```bash
    gpg --output your.name.gpg.asc --armor --export youname@domain.com
    gpg --send-keys --keyserver hkp://pgp.mit.edu last_8_key_pub_ring_digits # like this 8 digits 778E57C4
```

-   gathering email public key
```bash
    # PGP PUBLIC Key Structure
    # -----BEGIN PGP PUBLIC KEY BLOCK-----
    #			  base64 here
    # -----END PGP PUBLIC KEY BLOCK-----

    gpg --list-secret-keys --keyid-format LONG # select you email sec_ID 
    gpg --armor --export your_sec_ID
```

- trusting email
- restart gpg

```
    gpg --edit-key email_address
        > trust
        > Enter_trust_number
        > quit

    gpg-connect-agent /bye
```

- update trustdb

```bash
    gpg --update-trustdb
```

- key `fingerprint`

```bash
    gpg --finger last_8_key_pub_ring_digits
```

**Encryption/Decryption through openssl**

-   `openssl`
-   Encryption
```bash
echo "file_or_text_to_encrypt" | openssl aes-128-cbc -a -salt -k mypassword U2FsdGVkX1+K6tvItr9eEI4yC4nZPK8b6o4fc0DR/Vzh7HqpE96se8Fu/BhM314z
```

-   Decryption
```bash
echo U2FsdGVkX1+K6tvItr9eEI4yC4nZPK8b6o4fc0DR/Vzh7HqpE96se8Fu/BhM314z | openssl aes-128-cbc -a -d -salt -k mypassword "file_or_text_to_decrypt"
```


# Edit needed.

-   **verification needed**

    """
    To encrypt your script using Openssl:

    1. cat <your-script> | openssl aes-128-cbc -a -salt -k "specify-a-password" > yourscript.enc

    OR

    2. openssl aes-128-cbc -a-salt -in <path-to-your-script> -k "yourpassword"

    To decrypt a script using Openssl (notice the '-d'):

    1. cat yourscript.enc | openssl aes-128-cbc -a -d -salt -k "specify-a-password" > yourscript.dec

    OR

    2. openssl aes-128-cbc -a -d -salt -in <path-to-your-script> -k "yourpassword" > yourscript.dec
    """

-   **verification needed**

# Edit needed. 



**Magic Sound**
-   `aplay` 
```bash
    aplay /dev/urandom
```


**MPlayer Magic**

-   Mirror via front webcam in linux through `MPlayer`

```bash
    mplayer -tv driver=v4l2:width=300:height=300: -vo xv -vf mirror tv://  -geometry "99%:99%" -noborder -ontop
```

**Spliting tar file to specified parts**


-   `split` source.
-   Consider additional `zip` suffix for splited chunks.
-   use `numeric suffixes` starting at `0`, `not alphabetic` with `-d` flag.

```bash
    split --additional-suffix=.zip -d -b 1500m 'source-file.tar.gz' "splited-file.part-" && zip splited-file.part*
```

-   `split` source file to `1.5GB chunks`.
-   `gzip` each chunk in case of `checksum`.

```bash
    split -d -b 1500m 'source-file.tar.gz' "splited-file.part-" && gzip splited-file.part*
```

-   compress as `bzip` chunks.
-   remove compressed parts in place.

```bash
    tar -cjf  'source-file.tar.bz2' source-file-directory/ --remove-files
```

-   split as `numeric chunks`.
-   `bz2` Chunks.
-   `bzip2` in case of checksum.

```bash
    split -d -b 1500m 'source-file.tar.bz2' "splited-file.part-" && bzip2 splited-file.part*
```

-   `Extract` via `FFmpeg` 
```bash
    ffmpeg -i "concat:video1.mp4|video2.mp4|video3.mp4" -c copy unique_file.mp4
```

-   `concatenate` all `test.zip.001`, `test.zip.002`, etc files into `test.zip`.
-   extract with `unzip test.zip`
```bash
    cat test.zip* > ~/test.zip
```

-   Linux `unzip` utility doesn't `multipart zip` extraction.
-   test `extraction process`.
```bash
    zip -FF test.zip --out test-full.zip
    unzip test-full.zip
```

-   RAR Structure
-   `a`rchive
-   5 iis best level Compression
-   1900MB splited
-   default `R`ecovery `R`ecord `-rr`
-   -R path/to/directory/

```bash
rar a -m5 -v1900M -rr "FrontendMasters - JavaScript Performance.rar" -R FrontendMasters\ -\ JavaScript\ Performance/
```


**Renaming-phobia**

-   change files ending with `.zip.gz`.
-   to file with `.gz`
-   for all file in current directory ending with `.gz`.

```bash
    rename.ul -- .zip.gz ".gz" *.gz
```

```bash
rename 's/test.extra//g' *.fasta

rename 's/extra.test//;' *

rename "extra.test" "" *
```


-   rename with `mv`for each record from `find stdout`.
-   renames txt files starting with `brand-` to `category-`.
```bash
    find . -name '*txt' -exec bash -c ' mv $0 ${0/brand-/category-}' {} \;

    for filename in ./*; do mv "./$filename" "./$(echo "$filename" | sed -e 's/test.extra//g')";  done
```




```bash
for file in $(find . -name "Flutter*"); do    echo mv $file `echo $file | sed s/_20/_/g`; done
```

-   command to find recursively files {can be modified for folders too, change -type f}, and renamed them by specfying regex pattern
-   the find command argument is reversed, it does not accept global commands before the other parameters
-   for directory -d
```bash
find . -maxdepth [depth] -type f -name "[filepattern]" | while read FNAME; do mv "$FNAME" "${FNAME//search/replace}"; done
```

-   changing domains.php, domain.php to leads.php, lead.php
```bash
find .  -maxdepth [depth] -type f  -name "domain*.php" | while read FNAME; do mv "$FNAME" "${FNAME//domain/lead}"; done
```


-   Instead of above commands use `7z` it combine and do the rest
```bash
    7z x archive.zip.001
```

**Sort**

-   List the 20 largest files and folders under the current working directory.
-   make `du stdout` Human readable wih `-h` flag.

```bash
    du -mah | sort -nr | head -n 20 
```

**System**

-   know your System integerated information.
```bash
    dmidecode -t system
```

-   Know your system limits
```bash
    getconf -a | sort -k2nr | grep MAX
```

-   know your limits
```bash
    sudo -l
```

**Transfer without any hassle**

-   `h`uman readable | `P`rogress same as `--info=progress2` | ignore already downloaded files `u`nless new | `r`ecursive | `v`erbose 
```bash
rsync -rhPu --info=progress2 {{remote_host}}:{{path/to/remote_file}} {{path/to/local_file}} -vvvv

```


**Fix Corruptted Downloaded file Torrent/Magnet**

-   use `aria2` to check Integrity
-   run command in the `downloaded file directory`.
-   you must have the same old torrent, you have used before, in case of Hash.
```bash
    aira2c --chcek-integrity intended_torrent_file_for_fixing_corrupted_or_uncompleted_downloaded_file.torrent
```

**Mysql Granting Failed**

-   `Root Priviledge`
```bash
    sudo mysql -u root
```

-   manupilate mysql priviledges on system via `root` user

```bash
    CREATE USER 'root'@'127.0.0.1' IDENTIFIED BY '<thi$ Sh0uld be @ $trmin3#4$>';
    GRANT ALL PRIVILEGES ON * . * TO 'root'@'127.0.0.1';
    FLUSH PRIVILEGES;
```

-   `denote:` `Don't know this one is required or not.`
```bash
    CREATE USER 'root'@'localhost' IDENTIFIED BY '<thi$ Sh0uld be @ $trmin3#4$>';
    GRANT ALL PRIVILEGES ON * . * TO 'root'@'localhost';
    FLUSH PRIVILEGES;
```

**Shell Power**

-   Don't ever underestimate your shell
-   move 100 pics to one directory. (instead of all of them)
```bash
mv food/foodporn-{4000..4100} first_100_images
```

-   above piece of Code in `Python`
```python
import os

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

paths = []

destinations = ['source', 'training', 'validation', 'testing']
for destination in destinations:
    paths.append(input('Absolute path for {}: '.format(destination)))

files = next(os.walk(paths[0]))[2]

divided = list(chunks(files, len(files)//3))
divided = divided[:len(divided)-1] if len(divided) % 3 != 0 else divided

destination = 1
for d in divided[:len(divided)]:
    for file in d: 
        os.rename(os.path.join(paths[0], file), os.path.join(paths[destination], file))
    destination += 1
```

**emojies in bash**

```bash
for i in 1F6{0..4}{{0..9},{A..F}} ; do printf "\\\\U$i \U$i\n" ; done 
```

**Start DWM Automatic**

-   add below to .profile        
```bash
[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx
```

---

- connect to pptp connections through terminal with nmcli
```bash
sudo nmcli con up id connection_name --ask
sudo nmcli con down id connection_name
```

- another way would be
```bash
sudo -s
vim  /etc/ppp/peers/yourvpnconnection

# add these lines to 
# pty "pptp your_vpn_hostname --nolaunchpppd --debug"
# name your_username
# password your_password
# remotename PPTP
# require-mppe-128
# require-mschap-v2
# refuse-eap
# refuse-pap
# refuse-chap
# refuse-mschap
# noauth
# debug
# persist
# maxfail 0
# defaultroute
# replacedefaultroute
# usepeerdns
#################################################
####### Replace hostname, username and password
#################################################
chmod 600  /etc/ppp/peers/yourvpnconnection

pon yourvpnconnection
poff yourvpnconnection
```

```bash
sudo apt install pptp-linux
modprobe ppp_mppe
#################################################
# creating vpnserver in /etc/ppp/peers/vpnserver
#################################################
# pty "pptp host-or-IP  --nolaunchpppd"
# name your_client1_username
# password your_client_password
# remotename PPTP
# require-mope-128
#################################################

pppd call vpnserver

# log pptp data
cat /var/log/syslog | grep pptp 

ip route add 172.16.0.0/16 dev ppp0
```

---

-   ssh-copy-id for windows 
<!-- If you have no id_rsa.pub file you can create one like this: -->

```
ssh-keygen -t rsa
cat ~/.ssh/id_rsa.pub | ssh user@server "umask 077; test -d ~/.ssh || mkdir ~/.ssh ; cat >> ~/.ssh/authorized_keys"
```

---

**Windows Applications**

-   **verification needed!**
-   not sure what is it, but will figure it out.
```bash
    env WINEPREFIX="/home/$USER/.wine" wine C:\\\\windows\\\\command\\\\start.exe /Unix /home/$USER/.wine/dosdevices/c:/users/Public/Desktop/
```

-----

**Windows ease of use**

-   Start another Administrator Tab in windows terminal

```powershell "Start-Process -Verb RunAs cmd.exe '/c start wt.exe'"```

-   We can install `sudo` via Chocolatey
choco install sudo -y 


------

**‌Booting in *nix/Unix-like OSs**

-   Ventoy
-   dd
-   ecther
-   weousb (+ gparted)
-   Chainloader
-   Unebootin
-   Rufus
